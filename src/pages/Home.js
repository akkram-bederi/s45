import{Fragment} from 'react';

import AppNavbar from '../Components/AppNavbar'
import Banner from '../Components/Banner'
import Footer from '../Components/Footer'
import Card from '../Components/Card'

export default function Home(){
	return(

		<Fragment>
			<AppNavbar />
			<Banner />
			<Card />
			<Footer />
		</Fragment>
	)
}