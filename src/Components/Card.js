import {Card,Button} from 'react-bootstrap'

export default function Footer(){
	return(
		<Card className="mx-auto" style={{width:"50vw"}}>
		  <Card.Body>
		    <Card.Title>Sample Course</Card.Title>
		    <Card.Text>
		    <h6>Description:</h6>
		    <p>This is a sample course offering</p>
		    <h6>Price:</h6>
		    <p>PhP 40,000</p>
		    </Card.Text>
		    <Button variant="info">Enroll</Button>
		  </Card.Body>
		</Card>
		)
}
